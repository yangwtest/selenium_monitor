package seleniumsave;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;


public class FirstDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//声明一个火狐浏览器driver对象
		WebDriver driver= new FirefoxDriver();
		//打开360搜索
		driver.get("http://www.baidu.com/");
		//找到搜索框元素
		WebElement searchInput= driver.findElement(By.name("wd"));
		//向搜索框输入“selenium”
		searchInput.sendKeys("selenium");
		//找到搜索按钮
		WebElement searchButton= driver.findElement(By.id("su"));
		//点击搜索按钮
		searchButton.click();
		try {
			Thread.sleep(2000);
			}catch(InterruptedException e) {e.printStackTrace();
			}
		//跳转之后的页面关键字输入框元素
		WebElement keywordInput= driver.findElement(By.id("kw"));
		//验证输入框的内容是不是selenium
		Assert.assertEquals(keywordInput.getAttribute("value"), "selenium");

		//关闭浏览器
		driver.quit();

	}

}
