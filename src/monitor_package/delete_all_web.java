package monitor_package;

import java.awt.List;
import java.util.ArrayList;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class delete_all_web {
	WebDriver driver ;
	@BeforeTest
	  public void beforeTest11() throws Throwable {
		use_cfg1 use_cfg = new use_cfg1();
		String[] re = use_cfg.use_cfg();	
		System.out.println("re1="+re[0]);
		System.out.println("re2="+re[1]); 		
	 	
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();
		  driver.get("http://test.51idc.com/monitor");
			WebElement searchInput= driver.findElement(By.name("username"));
			searchInput.sendKeys(re[0]);
			
			WebElement searchInput2= driver.findElement(By.name("password"));
			searchInput2.sendKeys(re[1]);
			WebElement loginButton= driver.findElement(By.id("submit1"));
			loginButton.click();
			Thread.sleep(5000);
		
			
	  }
	  
	@Test
			public void delmonitor() throws InterruptedException {
			
			//进入站点监控
			WebElement createmonitor=driver.findElement(By.xpath("/html/body/div[2]/div/div/div/ul/li[3]/a/span"));	
			createmonitor.click();
			Thread.sleep(5000);
		
			//找到所有有删除图标的条数
			 WebElement deliron=driver.findElement(By.id("init_site_table"));
			 java.util.List<WebElement> element = deliron.findElements(By.xpath("//a[@class='add_peie del']"));
             int num = element.size();
             System.out.println("num="+num);
             
             
             
             int i;	
             //删除这些条，条数为num/2
             for( i=1;i<=num/2;i++)
             {
            	 if (num/2==0)
            	 {break;} 
            	 
	    		WebElement deliron1=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[2]/div/div/div/div/div[1]/div[2]/table/tbody/tr/td[8]/a[3]"));
	    		deliron1.click();
	    		Thread.sleep(5000);
					
		    	
				//点询问删除否的系统提示框	
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(15000);
				
				//点删除确定
		    	WebElement delconfirm=driver.findElement(By.id("btnConfirm"));	
		    	delconfirm.click();
				Thread.sleep(20000);
		 
	    	}	
	
	}

		  @AfterTest
		  public void afterTest() {
		  driver.quit();
		  
		  }
}
	
