package monitor_package;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class del_all_server {
	WebDriver driver ;
	@BeforeTest
	  public void beforeTest22() throws Throwable {
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();
		  driver.get("http://test.51idc.com/monitor");
			WebElement searchInput= driver.findElement(By.name("username"));
			searchInput.sendKeys("test3@51idc.com");
			WebElement searchInput2= driver.findElement(By.name("password"));
			searchInput2.sendKeys("123456");
			WebElement loginButton= driver.findElement(By.id("submit1"));
			loginButton.click();
			Thread.sleep(5000);
	  }
	  
			@Test
			public void delservermonitor() throws InterruptedException {
			
				//进入服务器监控
				WebElement servermonitor=driver.findElement(By.xpath("/html/body/div[2]/div/div/div/ul/li[4]/a/span"));	
				servermonitor.click();
				Thread.sleep(5000);
		
			//找到所有有删除图标的条数
			 WebElement deliron=driver.findElement(By.id("init_changg_table"));
			 java.util.List<WebElement> element = deliron.findElements(By.xpath("//a[@class='add_peie del']"));
             int num = element.size();
             System.out.println("num="+num);
                        
             
             int i;	
             //删除这些条，条数为num
             for( i=1;i<=num;i++)
             {
            	 if (num==0)
            	 {break;} 
            	 
	    		WebElement deliron1=driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/div/div[2]/div/div/div[2]/table/tbody/tr/td[9]/a[3]"));
	    		deliron1.click();                               
	    		Thread.sleep(5000);
					
		    	
				//点询问删除否的系统提示框	
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(15000);
				
				//点删除确定
		    	WebElement delconfirm=driver.findElement(By.id("btnConfirm"));	
		    	delconfirm.click();
				Thread.sleep(20000);
		 
	    	}	
	
	}
			@AfterTest
			public void afterTest() {
			driver.quit();	
			}
}
