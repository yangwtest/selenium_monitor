package monitor_package;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;

public class inter_win_server {
	WebDriver driver ;
//	private List<WebElement> selectallpath;
	@BeforeTest
	  public void beforeTest() throws InterruptedException {
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();
		  driver.get("http://test.51idc.com/monitor");
			WebElement searchInput= driver.findElement(By.name("username"));
			searchInput.sendKeys("yangw@51idc.com");
			WebElement searchInput2= driver.findElement(By.name("password"));
			searchInput2.sendKeys("Anchang123");
			WebElement loginButton= driver.findElement(By.id("submit1"));
			loginButton.click();
			Thread.sleep(5000);

	  }
		@Test
		public void server_monitor() throws InterruptedException {
		//进入服务器监控
		WebElement servermonitor=driver.findElement(By.xpath("/html/body/div[2]/div/div/div/ul/li[4]/a/span"));	
		servermonitor.click();
		Thread.sleep(5000);
		//进入创建服务器监控
		WebElement createservermonitor=driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/div/div[1]/ul/li/a"));	
		createservermonitor.click();
		Thread.sleep(15000);
		//点名称
		WebElement servername=driver.findElement(By.name("serverName"));	
		servername.clear();
		servername.sendKeys("221.228.88.44-window-inter");
		//URL
		WebElement url=driver.findElement(By.id("privateIp"));	
		//向搜索框输入“192.168.3.2”
		url.clear();
		url.sendKeys("192.168.3.2");
		
		//勾选择采集器
		WebElement selectcoll=driver.findElement(By.id("if_redec"));	
		selectcoll.click();
		Thread.sleep(5000);

		//点请选择监控点分组下拉框autotest0126
		Select selectAge = new Select(driver.findElement(By.id("pluginTaskUseListId")));  
		selectAge.selectByVisibleText("autotest_yangw_0201");
		Thread.sleep(5000);
    

		//SNMP IP
		WebElement snmpip=driver.findElement(By.id("serverIp"));	
		//向搜索框输入“192.168.3.2”
		snmpip.clear();
		snmpip.sendKeys("192.168.3.2");
	
		//os
		WebElement os=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[1]/div[2]/div/dl[1]/div/dd/div[4]/div/ul/li[2]/a"));	
		os.click();
		Thread.sleep(5000);
		//community
		WebElement community=driver.findElement(By.id("snmpComm"));
		community.clear();
		community.sendKeys("public");
		
		//WebElement selectall=driver.findElement(By.id("windcheck"));
		//List<WebElement> inputs = selectall.findElements(By.xpath("//input[@class='cbox selectwinall']"));
		//inputs.add(selectall);
		
		//点全选
		WebElement selectall=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[1]/div[2]/div/dl[2]/dd/div[3]/div/input"));	
		selectall.click();
		Thread.sleep(5000);
		//CPU使用率
		WebElement cpuuser=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[2]/div[2]/div/div/div/dl[2]/dd/div/div/ul/li[2]"));	
		cpuuser.click();
		Thread.sleep(5000);
		//阀值
		WebElement cpuvalue=driver.findElement(By.id("input_value_7"));
		cpuvalue.clear();
		cpuvalue.sendKeys("50");
		//点确定
		WebElement cpucfn=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[2]/div[2]/div/div/div/dl[2]/div[1]/div[2]/div/div/a[2]"));	
		cpucfn.click();
		Thread.sleep(5000);
		
		
		//点提交
		WebElement submit=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[3]/a/span"));	
		submit.click();
		Thread.sleep(15000);
		
		//点提交确定
		WebElement confirmok=driver.findElement(By.id("btnConfirm"));	
		confirmok.click();
		Thread.sleep(21000);
		
		
		
		
		//modify
				//点修改图标
				WebElement selectmodify=driver.findElement(By.id("init_changg_table"));	
				java.util.List<WebElement> element = selectmodify.findElements(By.xpath("//a[@class='add_peie del']"));
				
				int num = element.size();
				System.out.println("num="+num);
				
				if (num==0)
					{				
					WebElement selectm=driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/div/div[2]/div/div/div[2]/table/tbody/tr/td[9]/a[2]"+num+"]/td[9]/a[2]"));
					selectm.click();
					Thread.sleep(15000);
				}
				else{
				//修改最后一个服务器监控项
					WebElement selectm=driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/div/div[2]/div/div/div[2]/table/tbody/tr["+num+"]/td[9]/a[2]"));
					selectm.click();
					Thread.sleep(15000);		
				};
				
				
			
				//修改名称
				WebElement httprename=driver.findElement(By.name("serverName"));	
				//String data = httprename.getText();
				
				JavascriptExecutor js = (JavascriptExecutor)driver;
				String data = (String) js.executeScript("return document.getElementById('serverName').value");
				System.out.println("data="+data);
				httprename.clear();
				httprename.sendKeys("mdf"+data);	

		
    	//再点提交创建
		WebElement resubmitcreate=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div[2]/div/div[3]/a/span"));	
		resubmitcreate.click();                               
		Thread.sleep(15000);
		
		//再点创建成功弹出框
		WebElement reselectsession=driver.findElement(By.id("btnConfirm"));	
		reselectsession.click();
		Thread.sleep(20000);


		/*
		//点删除图标
    	WebElement deliron=driver.findElement(By.xpath("/html/body/div[4]/div/div/div[1]/div/div[2]/div/div/div[2]/table/tbody/tr/td[9]/a[3]"));	
    	deliron.click();                                
		Thread.sleep(5000);
    	
		//点询问删除否的系统提示框	
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Thread.sleep(15000);
		
		//点删除确定
    	WebElement delconfirm=driver.findElement(By.id("btnConfirm"));	
    	delconfirm.click();
		Thread.sleep(15000);
		*/
	}
		@AfterTest
		public void afterTest() {
		driver.quit();	
		}
}

