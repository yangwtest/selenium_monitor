package monitor_package;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
public class create_web_monitor {
	WebDriver driver ;
	@BeforeTest
	  public void beforeTest() throws Exception {
		use_cfg1 use_cfg = new use_cfg1();
		String[] re = use_cfg.use_cfg();
		  
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();
		  driver.get("http://test.51idc.com/monitor");
			
			WebElement searchInput= driver.findElement(By.name("username"));
			searchInput.sendKeys(re[0]);
			WebElement searchInput2= driver.findElement(By.name("password"));
			searchInput2.sendKeys(re[1]);
			WebElement loginButton= driver.findElement(By.id("submit1"));
			loginButton.click();
			Thread.sleep(5000);
	  }
	@Test
	public void create_web_monitor() throws Exception {
		use_cfg1 use_cfg = new use_cfg1();
		String[] re = use_cfg.use_cfg();
  		//进入站点监控
		WebElement createmonitor=driver.findElement(By.xpath("/html/body/div[2]/div/div/div/ul/li[3]/a/span"));	
		createmonitor.click();
		Thread.sleep(5000);
		
		
		for(int i=1;i<3;i++){
		//1 HTTP
		//点创建监控
		WebElement createweb=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
		createweb.click();
		Thread.sleep(5000);
		//点创建HTTP监控
		WebElement createhttp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[1]/div/a"));	
		createhttp.click();
		Thread.sleep(5000);
		//点HTTP名称
		WebElement httpname=driver.findElement(By.name("taskName"));	
		//向搜索框输入名称加I
		httpname.clear();
		httpname.sendKeys("http汉字"+i);
		//URL
		WebElement url=driver.findElement(By.name("url"));	
		//向搜索框输入“http://www.baidu.com”
		url.clear();
		url.sendKeys("http://www.baidu.com");
		
		//点请选择监控点分组下拉框10
		Select selectAge = new Select(driver.findElement(By.id("mg_name")));  
		selectAge.selectByValue(re[2]); 
		Thread.sleep(5000);
    
    	//点10
		WebElement selectop=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
		selectop.click();                                
		Thread.sleep(5000);
	
    	//点提交创建
		WebElement submitcreate=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
		submitcreate.click();
		Thread.sleep(5000);
		
		//点创建成功弹出框
		WebElement selectsession=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
		selectsession.click();
		Thread.sleep(15000);
		
		
		
		
		
		//2ping
				//点创建监控
				WebElement createweb2=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb2.click();
				Thread.sleep(5000);
				//点创建ping监控
				WebElement createping=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[2]/div/a/i"));	
				createping.click();
				Thread.sleep(5000);
				//点ping名称
				WebElement pingname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				pingname.clear();
				pingname.sendKeys("ping安畅"+i);
				//URL
				WebElement urlping=driver.findElement(By.id("host"));	
				//向搜索框输入“www.baidu.com”
				urlping.clear();
				urlping.sendKeys("www.baidu.com");
				
				//点请选择监控点分组下拉框10
				Select selectAge2 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge2.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop2=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop2.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate2=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate2.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession2=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession2.click();
				Thread.sleep(15000);
				
				
				
			//3ftp
				//点创建监控
				WebElement createweb3=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb3.click();
				Thread.sleep(5000);
				//点创建ftp监控
				WebElement createftp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[3]/div/a/i"));	
				createftp.click();
				Thread.sleep(5000);
				//点ftp名称
				WebElement ftpname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				ftpname.clear();
				ftpname.sendKeys("ftp新年"+i);
				//URL
				WebElement urlftp=driver.findElement(By.id("host"));	
				//向搜索框输入“www.baidu.com”
				urlftp.clear();
				urlftp.sendKeys("www.baidu.com");
				
				//选匿名登录
				WebElement selectone=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[1]/div[2]/div/div[4]/div[1]/input[2]"));	
				selectone.click();
				Thread.sleep(5000);		
				
				//点请选择监控点分组下拉框10
				Select selectAge3 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge3.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop3=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop3.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate3=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate3.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession3=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession3.click();
				Thread.sleep(15000);			
		
		//4dns
				//点创建监控
				WebElement createweb4=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb4.click();
				Thread.sleep(5000);
				//点创建dns监控
				WebElement createdns=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[4]/div/a/i"));	
				createdns.click();
				Thread.sleep(5000);
				//点dns名称
				WebElement dnsname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				dnsname.clear();
				dnsname.sendKeys("dns网络"+i);
				//URL
				WebElement urldns=driver.findElement(By.id("domain"));	
				//向搜索框输入“www.baidu.com”
				urldns.clear();
				urldns.sendKeys("www.baidu.com");
				
				//点请选择监控点分组下拉框10
				Select selectAge4 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge4.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop4=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop4.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate4=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate4.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession4=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession4.click();
				Thread.sleep(15000);
				
		//5tcp
				//点创建监控
				WebElement createweb5=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb5.click();
				Thread.sleep(5000);
				//点创建tcp监控
				WebElement createtcp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[5]/div/a/i"));	
				createtcp.click();
				Thread.sleep(5000);
				//点tcp名称
				WebElement tcpname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				tcpname.clear();
				tcpname.sendKeys("tcp互联网"+i);
				//URL
				WebElement urltcp=driver.findElement(By.id("host"));	
				//向搜索框输入“www.baidu.com”
				urltcp.clear();
				urltcp.sendKeys("www.baidu.com");
				
				//port 80
				WebElement tcpport=driver.findElement(By.id("tcpPort"));	
				tcpport.clear();
				tcpport.sendKeys("80");
				
				//点请选择监控点分组下拉框10
				Select selectAge5 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge5.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop5=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop5.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate5=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate5.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession5=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession5.click();
				Thread.sleep(15000);
				
				
				
		//6udp
				//点创建监控
				WebElement createweb6=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb6.click();
				Thread.sleep(5000);
				//点创建udp监控
				WebElement createudp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[6]/div/a/i"));	
				createudp.click();
				Thread.sleep(5000);
				//点udp名称
				WebElement udpname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				udpname.clear();
				udpname.sendKeys("udp云服务"+i);
				//URL
				WebElement urludp=driver.findElement(By.id("host"));	
				//向搜索框输入“www.baidu.com”
				urludp.clear();
				urludp.sendKeys("www.baidu.com");
				
				//port 53
				WebElement udpport=driver.findElement(By.id("udpPort"));	
				udpport.clear();
				udpport.sendKeys("53");
				
				//requestStr
				WebElement requestStr=driver.findElement(By.id("requestStr"));	
				requestStr.clear();
				requestStr.sendKeys("0x01");
				
				//点请选择监控点分组下拉框10
				Select selectAge6 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge6.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop6=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop6.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate6=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate6.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession6=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession6.click();
				Thread.sleep(15000);
				
				
		//7smtp
				//点创建监控
				WebElement createweb7=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
				createweb7.click();
				Thread.sleep(5000);
				//点创建smtp监控
				WebElement createsmtp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[7]/div/a/i"));	
				createsmtp.click();
				Thread.sleep(5000);
				//点smtp名称
				WebElement smtpname=driver.findElement(By.name("taskName"));	
				//向搜索框输入名称加I
				smtpname.clear();
				smtpname.sendKeys("smtp上市"+i);
				//URL
				WebElement urlsmtp=driver.findElement(By.id("host"));	
				//向搜索框输入“www.baidu.com”
				urlsmtp.clear();
				urlsmtp.sendKeys("www.baidu.com");
				
				//port 25
				WebElement smtpport=driver.findElement(By.id("smtpPort"));	
				smtpport.clear();
				smtpport.sendKeys("25");
								
				//点请选择监控点分组下拉框10
				Select selectAge7 = new Select(driver.findElement(By.id("mg_name")));  
				selectAge7.selectByValue(re[2]); 
				Thread.sleep(5000);
		    
		    	//点10
				WebElement selectop7=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
				selectop7.click();                                
				Thread.sleep(5000);
			
		    	//点提交创建
				WebElement submitcreate7=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
				submitcreate7.click();
				Thread.sleep(5000);
				
				//点创建成功弹出框
				WebElement selectsession7=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
				selectsession7.click();
				Thread.sleep(15000);
		}
	}
	@AfterTest
	public void afterTest() {
	driver.quit();
	
	}
}