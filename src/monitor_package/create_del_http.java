package monitor_package;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class create_del_http {
	WebDriver driver ;
	@BeforeTest
	  public void beforeTest() throws Exception {
		use_cfg1 use_cfg = new use_cfg1();
		String[] re = use_cfg.use_cfg();
		
		  driver = new FirefoxDriver();
		  driver.manage().window().maximize();
		  driver.get("http://test.51idc.com/monitor");
			WebElement searchInput= driver.findElement(By.name("username"));
			searchInput.sendKeys(re[0]);
			WebElement searchInput2= driver.findElement(By.name("password"));
			searchInput2.sendKeys(re[1]);
			WebElement loginButton= driver.findElement(By.id("submit1"));
			loginButton.click();
			Thread.sleep(5000);
	  }
	
	@Test
	public void entermonitor() throws Exception {
		use_cfg1 use_cfg = new use_cfg1();
		String[] re = use_cfg.use_cfg();	
		
		//for (int i=1;i<2;i++){
		//进入站点监控
		WebElement create1monitor=driver.findElement(By.xpath("/html/body/div[2]/div/div/div/ul/li[3]/a/span"));	
		create1monitor.click();
		Thread.sleep(5000);
		//点创建监控
		WebElement createweb=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[1]/ul/li[1]/a"));	
		createweb.click();
		Thread.sleep(5000);
		//点创建HTTP监控
		WebElement createhttp=driver.findElement(By.xpath("/html/body/div[4]/div/div/div/div/div[2]/div/div/ul/li[1]/div/a"));	
		createhttp.click();
		Thread.sleep(5000);
		//点HTTP名称
		WebElement httpname=driver.findElement(By.name("taskName"));	
		//向搜索框输入名称加I
		httpname.clear();
		httpname.sendKeys("httpfordel");
		//URL
		WebElement url=driver.findElement(By.name("url"));	
		//向搜索框输入“http://www.baidu.com”
		url.clear();
		url.sendKeys("http://www.baidu.com");
		
		//点请选择监控点分组下拉框10
		Select selectAge = new Select(driver.findElement(By.id("mg_name")));  
		selectAge.selectByValue(re[2]); 
		Thread.sleep(5000);
    
    	//点10
		WebElement selectop=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[2]/div[2]/div/div/div/dl/dd/div[2]/div/select/option["+re[3]+"]"));	
		selectop.click();
		Thread.sleep(5000);
	
    	//点提交创建
		WebElement submitcreate=driver.findElement(By.xpath("/html/body/div[4]/div/div/form/div/div/div/div/div[3]/a[2]/span"));	
		submitcreate.click();
		Thread.sleep(5000);
		
		//点创建成功弹出框
		WebElement selectsession=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/form/fieldset/div/a"));	
		selectsession.click();
		Thread.sleep(15000);
		
		//判断页面监控项目是否创建成功，通过名称来判断
		//By locator = By.linkText("httpfordel");
		//System.out.println(locator);
	    //boolean re = isElementExsit(driver,locator);   
	    //System.out.println("confirm"+re);
	    
	    //if (re=true)
	    //{
	    	//点删除图标
	    	WebElement deliron=driver.findElement(By.xpath("/html/body/div[4]/div/form/div/div[2]/div/div/div/div/div[1]/div[2]/table/tbody/tr/td[8]/a[3]"));	
	    	deliron.click();
			Thread.sleep(5000);
	    	
			//点询问删除否的系统提示框	
			Alert alert = driver.switchTo().alert();
			alert.accept();
			Thread.sleep(15000);
			
			//点删除确定
	    	WebElement delconfirm=driver.findElement(By.id("btnConfirm"));	
	    	delconfirm.click();
			Thread.sleep(15000);
    
	    }
	    
	    
 // }
//}
/*	@Test
	public static boolean isElementExsit(WebDriver driver, By locator) {
		 boolean flag = false;  
	        try {  
	            WebElement element=driver.findElement(locator);  
	            flag=null!=element;  
	        } catch (NoSuchElementException e) {  
	            System.out.println("Element:" + locator.toString()  
	                    + " is not exsit!");  
	        }  
	        return flag;
	}
	*/	
		@AfterTest
	  	public void afterTest() {
		driver.quit();
	  }

}
